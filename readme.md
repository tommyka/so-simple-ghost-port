Port of [so simple](https://mmistakes.github.io/so-simple-theme/articles/readability-feature-post/) theme for Jekyll to ghost 

Fonts are provided from [Adobe edge fonts](https://edgewebfonts.adobe.com/fonts#/?nameFilter=source&languages=en&collection=volkhov:i4;source-sans-pro:n4,n6,n7)